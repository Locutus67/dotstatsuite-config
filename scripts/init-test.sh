#!/bin/sh 
DIR_CONFIGS='./configs'

if [[ -d $DIR_CONFIGS ]]; then
  cp -r data/dev/configs/test $DIR_CONFIGS/
else
  cp -r data/dev/configs/ $DIR_CONFIGS
fi
