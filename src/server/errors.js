export function HTTPError(code) {
  Error.captureStackTrace(this, HTTPError);
  this.name = 'HTTPError';
  this.code = code;
}
HTTPError.prototype = Object.create(Error.prototype);
HTTPError.prototype.constructor = HTTPError;
