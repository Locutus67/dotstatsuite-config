import GoogleProvider from './googleProvider';
import MinioProvider from './minioProvider';
import FileSystemProvider from './fileSystemProvider';

export default ctx => {
  const { config } = ctx();

  switch(config.bucketsProvider.name){
    case 'minio': return MinioProvider(config)
    case 'gke': return GoogleProvider(config)
    case 'fs': return FileSystemProvider(config)
  }
}
