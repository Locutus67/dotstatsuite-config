import debug from '../debug';
import { HTTPError } from '../errors';
const Minio = require('minio');

const downloadFile = (minio, bucketname) => id => {
  return new Promise((resolve, reject) => {
    minio.getObject(bucketname, id, (err, stream) => {
      if (err){
        if(err.code === 'NoSuchKey') reject(new HTTPError(404));
        if(err.code === 'NoSuchBucket') reject(new HTTPError(404));

        return reject(err);
      }
      resolve(stream);
    });
  });
};

export default config => {
  const conf = {
    endPoint: config.bucketsProvider.endpoint,
    port: config.bucketsProvider.port || 9000,
    accessKey: config.bucketsProvider.accessKey,
    secretKey: config.bucketsProvider.secretKey,
    useSSL: false,
  };

  const minio = new Minio.Client(conf);
  debug.info(`ready to store data in buckets on minio`);

  return bucketname => ({
    name: 'MinioProvider',
    downloadFile: downloadFile(minio, bucketname),
  });
};
