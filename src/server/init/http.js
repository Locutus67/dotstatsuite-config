import debug from '../debug';

const getUrl = server => `http://${server.address().address}:${server.address().port}`;

export default ctx => {
  const {
    app,
    config: {
      server: { host = 'localhost', port },
    },
  } = ctx();
  return new Promise((resolve, reject) => {
    const server = app.listen(port, host, err => {
      if (err) return reject(err);
      const url = getUrl(server);
      debug.info(`Server started on ${url}`);
      server.url = url;
      resolve(ctx({ httpServer: server }));
    });
  });
};

