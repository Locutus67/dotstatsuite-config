import express from 'express';
import { replace, find, map, compose, fromPairs, toPairs } from 'ramda';
import  mimetype  from 'mime-types'
import debug from '../debug';
import { HTTPError } from '../errors';
import errorHandler from '../middlewares/errors';


// const expressPino = require('express-pino-logger')({ logger: debug });

const xFormSpace = ([id, space]) => [id, { id, ...space }];
const xFormSpaces = compose(fromPairs, map(xFormSpace), toPairs);
const xFormTenant = ([id, tenant]) => [id, { id, ...tenant, spaces: tenant.spaces && xFormSpaces(tenant.spaces) }];
const xFormTenants = compose(fromPairs, map(xFormTenant), toPairs);


const router = config => async (req, res, next) => {
  const matchRoute = ({ url }) => {
    if (url === '*') return true;
    return req.url.match(url);
  };
  const {
    url,
  } = req;
  const route = find(matchRoute)(config);
  if (!route) return next();
  const id = replace(route.url, '/', url)
  const uri = `${route.provider.name}::${id}`;
  try{
    const type = mimetype.lookup(url)
    res.type(type);
  }catch(err){
    debug.warn(`Unknown mimetype for ${url}`)
  }
  if(id === '/') return next()
  debug.info(`"${url}" => "${uri}"`);
  try{
    const stream = await route.provider.downloadFile(id)
    stream.on('error', err => {
      if(err.code === 'ENOENT') return next(new HTTPError(404))
      next(err)
    });
    if(id === '/tenants.json') {
      const chunks = [];
      stream.on('data', chunk =>  chunks.push(chunk));
      stream.on('end', () => {
        const body = Buffer.concat(chunks);
        res.json(xFormTenants(JSON.parse(body)));
      });
    }else{
      stream.pipe(res);
    }
    debug.info(`"${url}" => "${uri}"`);
  }catch(err){
    next(err);
  }
};

const startTime = new Date();

export default ctx => {
  const app = express();
  const {
    config: { gitHash, buckets },
    bucketProvider,
  } = ctx();
  app.use('/ping', (req, res) => res.json({ ping: 'pong' }));
  app.use('/healthcheck', (req, res) => res.json({ gitHash, startTime }));
  // app.use(expressPino);
  const routes = [
    { url: /^\/assets\//, provider: bucketProvider(buckets.assets) },
    { url: /^\/configs\//, provider: bucketProvider(buckets.configs)  },
    { url: /^\/i18n\//, provider: bucketProvider(buckets.i18n)  },
  ];

  app.use(router(routes));
  app.use((req, res, next) => {
    debug.info(`"${req.url}" => Unknown route`)
    next();
  });
  app.use(errorHandler);

  return ctx({ app });
};

