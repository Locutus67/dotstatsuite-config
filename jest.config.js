module.exports = {
  verbose: true,
  collectCoverageFrom: ['src/**/*.js', '!src/**/*.test.{js,jsx}'],
  setupFilesAfterEnv: ['<rootDir>/jest-setup.js'],
  testRegex: 'tests/.*\\.test\\.js$',
};
